public without sharing class ContactController {
    
    @AuraEnabled(cacheable=true)
    public static List<Contact> getRelatedContacts(){
        return [select Id,Name, Phone, Email ,Account.Name from Contact ];
    }


    @AuraEnabled(cacheable=true)
    public static List<Contact> getRelatedContactsByName(String searchKey){
        String key = '%' + searchKey + '%';
        return [select Name, Phone, Email ,accountName__c from Contact where Name LIKE :key ];
    }


    @AuraEnabled(cacheable=true)
    public static List<Contact> getRelatedFeedback(String searchKey){
        String key = '%' + searchKey + '%';
        return [select Name, Phone, Email ,Description from Contact where Name LIKE :key ];
    }


    @AuraEnabled(cacheable=true)
    public static List<Contact> getRelatedContactsByFilter(Id accountId,String key){
        String query='select Id,Name, Phone, Email from Contact where AccountId=:accountId and Name like \'%'+key+'%\'';
        return Database.query(query);
    }
    @AuraEnabled(cacheable=true)
    public static List<Contact> findContactByAccountId(String accountId) {      
        return [
            SELECT Id, FirstName,LastName,Email,Phone  
            FROM Contact 
            WHERE AccountId=:accountId 

            LIMIT 10];
    }

      @AuraEnabled(cacheable=true)
    public static List<Contact> getContacts(){
        return [select Id,Name, Phone, Email from Contact ];
    }

    @AuraEnabled(cacheable=true)
    public static List<Contact> retrieveContactData(string keySearch){       
        
        List<Contact> contactList = [Select Id, FirstName, LastName, Email, Phone, Account.Name From Contact Where Account.Name=:keySearch];
        return contactList;
    }



    
    @AuraEnabled
    public static list<Contact> readCSVFileContact(Id idContentDocument){
        list<Contact> lstAccsToInsert = new list<Contact>();
        if(idContentDocument != null) {
            
            // getting File Data based on document id 
            ContentVersion objVersion = [SELECT Id, VersionData FROM ContentVersion WHERE ContentDocumentId =:idContentDocument];
            // split the file data
            list<String> lstCSVLines = objVersion.VersionData.toString().split('\n');

            for(Integer i = 1; i < lstCSVLines.size(); i++){
                Contact objAcc = new Contact();
                list<String> csvRowData = lstCSVLines[i].split(',');
                System.debug('csvRowData====> '+csvRowData);
                
                objAcc.LastName = csvRowData[0]; // accName
                objAcc.Phone = csvRowData[1];
                objAcc.Email = csvRowData[2];
              

               
            
                lstAccsToInsert.add(objAcc);
            }

            try{    
                if(!lstAccsToInsert.isEmpty()) {
                    insert lstAccsToInsert;
                }
            }
            catch (Exception ex) {
                throw new AuraHandledException(ex.getMessage());
            } 
        }
        return lstAccsToInsert;    
    }
    @AuraEnabled(cacheable=true)
    public static List<Contact> retrieveContactCase(string searchKey){       
        String key = '%' + searchKey + '%';
         return [Select  Name, Email, Phone, Description,accountName__c From Contact Where case__c != null AND Name LIKE :key ];
   
    }

    @AuraEnabled(cacheable=true)
    public static List<Contact> getCasess(){
        //String key = '%' + searchKey + '%';
         return [SELECT   Name, Email, Phone, Description,accountName__c From Contact Where case__c != null  ];
}
}