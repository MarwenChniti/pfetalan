public with sharing class ContractController {
    
    @AuraEnabled(cacheable=true)
   public static List<Schema.Contract> getRelatedContracts(String searchKey){
        String key = '%' + searchKey + '%';
        return [select ContractNumber,accountName__c,Status,ContractTerm,StartDate,EndDate from Contract where Status LIKE :key ];
   }



    @AuraEnabled(cacheable=true)
 public static List<Schema.APXT_CongaSign__Transaction__c> getRelatedTransactions(String searchKey){
        String key = '%' + searchKey + '%';
        return [select 	Name,Contract_Number__c,APXT_CongaSign__Status__c, APXT_CongaSign__RequestedOn__c,	APXT_CongaSign__CompletedOn__c from APXT_CongaSign__Transaction__c  where APXT_CongaSign__Status__c LIKE :key ];
   }
 
}
