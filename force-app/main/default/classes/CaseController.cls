public with sharing class CaseController {

    @AuraEnabled(cacheable=true)
    public static List<Schema.Case> getRelatedCases(String searchKey){
        String key = '%' + searchKey + '%';
        return [select 	CaseNumber,Subject,Status,Priority,AccountName__c,ContactName__c,CreatedDate from Case where Status LIKE :key ];
    }

    @AuraEnabled
    public static void CaseController(String Id ) {
             System.debug('test1'+Id);

       // String key = '%' + Id + '%';
        List <Schema.Case> ls = new List<Schema.Case>();
        ls = [SELECT Id FROM Case];
        for ( Schema.Case c : ls ){
            if (c.Id == Id)
            c.Status='Working' ;

        }
    // c.Status='Approved' ;
    // System.debug('test1'+c);
     update ls ;
   
    }
}
