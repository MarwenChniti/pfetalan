public with sharing class ProductController {

    @AuraEnabled(cacheable=true)
    public static List<Product2> getProductList() {
        return [
            SELECT  Name, Description, Family,IsActive, ProductCode , image__c
            FROM Product2
            
            
        ];
    }

    @AuraEnabled(cacheable=true)
    public static List<Product2> getProduct(String searchKey) {
        String key = '%' + searchKey + '%';
        return [ SELECT  Name, Description, Family,IsActive, ProductCode FROM Product2 where Name LIKE :key ];
    
 }

 @AuraEnabled(cacheable=true)
 public static List<Product2> getproudctss(){
     //String key = '%' + searchKey + '%';
      return [SELECT  Name, Description,ProductCode FROM Product2 ];
 }

    public ProductController() {

    }

    @AuraEnabled
    public static list<Product2> readCSVFileProduct(Id idContentDocument){
        list<Product2> lstAccsToInsert = new list<Product2>();
        if(idContentDocument != null) {
            
            // getting File Data based on document id 
            ContentVersion objVersion = [SELECT Id, VersionData FROM ContentVersion WHERE ContentDocumentId =:idContentDocument];
            // split the file data
            list<String> lstCSVLines = objVersion.VersionData.toString().split('\n');

            for(Integer i = 1; i < lstCSVLines.size(); i++){
                Product2 objAcc = new Product2();
                list<String> csvRowData = lstCSVLines[i].split(',');
                System.debug('csvRowData====> '+csvRowData);
                
                objAcc.Name = csvRowData[0]; // accName
                objAcc.Description = csvRowData[1];
                objAcc.ProductCode = csvRowData[2];
                objAcc.image__c = csvRowData[3];
                
              
                lstAccsToInsert.add(objAcc);
            }

            try{    
                if(!lstAccsToInsert.isEmpty()) {
                    insert lstAccsToInsert;
                }
            }
            catch (Exception ex) {
                throw new AuraHandledException(ex.getMessage());
            } 
        }
        return lstAccsToInsert;    
    }
}
