public with sharing class OpportunityController {
    @AuraEnabled(cacheable=true)
    public static List<Opportunity> getRelatedOpportunities(String searchKey){
        String key = '%' + searchKey + '%';
        return [select Name,StageName, CloseDate,Amount,accountName__c from Opportunity where Name LIKE :key ];
    }

    @AuraEnabled
    public static void changeOppDevReason(String oppId,String reason){
        System.debug('a'+oppId);
        System.debug ('b'+reason);
        list<Opportunity> ls = [Select Id,reason_for_refusal__c from Opportunity];
        list<Opportunity> lss = new List<Opportunity>();
        for (Opportunity o : ls ){
            if (o.Id == oppId){
                o.reason_for_refusal__c = reason;
                lss.add(o);
            }
        }
        update (lss);
    }
    
    @AuraEnabled
    public static list<Opportunity> readCSVFileOpp(Id idContentDocument){
        list<Opportunity> lstAccsToInsert = new list<Opportunity>();
        if(idContentDocument != null) {
            
            // getting File Data based on document id 
            ContentVersion objVersion = [SELECT Id, VersionData FROM ContentVersion WHERE ContentDocumentId =:idContentDocument];
            // split the file data
            list<String> lstCSVLines = objVersion.VersionData.toString().split('\n');

            for(Integer i = 1; i < lstCSVLines.size(); i++){
                Opportunity objAcc = new Opportunity();
                list<String> csvRowData = lstCSVLines[i].split(',');

                System.debug('csvRowData====> '+csvRowData);
                
                objAcc.Name = csvRowData[0]; // accName
                objAcc.StageName = csvRowData[1];
                objAcc.CloseDate = Date.valueOf(csvRowData[2]) ;
                objAcc.Amount =Decimal.valueOf(csvRowData[3]);
                
               // objAcc.accountName__c = csvRowData[4];
              
              

               
            
                lstAccsToInsert.add(objAcc);
            }

            try{    
                if(!lstAccsToInsert.isEmpty()) {
                    insert lstAccsToInsert;
                    
                }
            }
            catch (Exception ex) {
                throw new AuraHandledException(ex.getMessage());
            } 
        }
        return lstAccsToInsert;    
    }
}
