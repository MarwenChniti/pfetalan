public with sharing class AccountController {
    public String accountId{get;set;}
    public Account actObj{get;set;}
    public AccountController() {
        actObj = new Account();
    }  
    @AuraEnabled(cacheable=true)    
    public static List<Account> getAccounts(String actName){
        String keyString = '%'+actName+'%';
        return [select id,name,description,website,phone,industry from Account where name like:keyString ];
    }
    @AuraEnabled
    public static List<Account> getAccountRecordsList(){
        return [select id,name,type,phone,industry from Account limit 10];
    }
    
    //public pageReference retrieveAccountDetail(){
      //  if(accountId!=null){
        //     actObj = [select id,name,type,industry from Account where id=:accountId];
            
        //}       
	//	return null;        
    //}
    @AuraEnabled(cacheable=true)
    public static List<Account>  getAcc(String actName){
        String keyString = '%'+actName+'%';
        return [select id,name from Account where name like:keyString];
    }

    @AuraEnabled(cacheable=true)    
    public static List<Account> getAccountss(){
        List<Account> accountlist = [select id,name from Account ];
       return  accountlist;
    }

}