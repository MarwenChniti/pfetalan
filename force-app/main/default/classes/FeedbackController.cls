public with sharing class FeedbackController {
  
    @AuraEnabled(cacheable=true)
    public static List<feedback__c> getRelatedFeedback(String searchKey){
        String key = '%' + searchKey + '%';
        return [select Name,contactName__c,rate1__c ,rate2__c,rate3__c,rate4__c from feedback__c where Name LIKE :key ];
    }
}
