public with sharing class priceBook {
    public priceBook() {

    }

    @AuraEnabled(cacheable=true)
    public static List<Pricebook2> getPriceBook(String searchKey) {
        String key = '%' + searchKey + '%';
        return [select Name,Description,IsActive from Pricebook2  where Name LIKE :key ];
    


    }



}
