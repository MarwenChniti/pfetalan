public with sharing class LeadController {
    @AuraEnabled(cacheable=true)
    public static List<Lead> getRelatedLeads(String searchKey){
        String key = '%' + searchKey + '%';
        return [select Name,Title,Email,Company,LeadSource , Rating , Phone  from Lead where Name LIKE :key ];
    }

    @AuraEnabled
    public static list<Lead> readCSVFileLead(Id idContentDocument){
        list<Lead> lstAccsToInsert = new list<Lead>();
        if(idContentDocument != null) {
            
            // getting File Data based on document id 
            ContentVersion objVersion = [SELECT Id, VersionData FROM ContentVersion WHERE ContentDocumentId =:idContentDocument];
            // split the file data
            list<String> lstCSVLines = objVersion.VersionData.toString().split('\n');

            for(Integer i = 1; i < lstCSVLines.size(); i++){
                Lead objAcc = new Lead();
                list<String> csvRowData = lstCSVLines[i].split(',');
                System.debug('csvRowData====> '+csvRowData);
                
                objAcc.LastName = csvRowData[0]; // accName
                objAcc.Title = csvRowData[1];
                objAcc.Email = csvRowData[2];
                objAcc.Company = csvRowData[3];
                objAcc.LeadSource = csvRowData[4];
                objAcc.Phone = csvRowData[5];
                objAcc.Rating = csvRowData[6];
              

               
            
                lstAccsToInsert.add(objAcc);
            }

            try{    
                if(!lstAccsToInsert.isEmpty()) {
                    insert lstAccsToInsert;
                }
            }
            catch (Exception ex) {
                throw new AuraHandledException(ex.getMessage());
            } 
        }
        return lstAccsToInsert;    
    }
}
