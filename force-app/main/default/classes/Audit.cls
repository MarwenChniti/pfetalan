public with sharing class Audit {

    @AuraEnabled(cacheable=true)
    public static List<Schema.Audit__c> getAudits(String searchKey){
        String key = '%' + searchKey + '%';
        return [SELECT id,  name_of_field__c, name_of_object__c, new_value__c ,old_value__c,type_of_change__c,type_of_object__c,date__c , user_audit__c  from Audit__c where user_audit__c LIKE :key ];
    }

    @AuraEnabled(cacheable=true)
    public static List<Audit__c> getAuditss(){
        //String key = '%' + searchKey + '%';
         return [SELECT  type_of_change__c , type_of_object__c , date__c , user_audit__c from Audit__c  ];
       // return ls;
        // List <marwen> lss = new List<marwen>();
        // for (Audit__c c : ls ){
        //     marwen m = new marwen();
        //     m.Id = (string) o.Id;
        //     m.name_of_object__c = o.name_of_object__c;
        //     m.type_of_change__c = o.type_of_change__c;
        //     lss.add(m);
        // }
        // return lss;
    }




    public Audit() {

    }
}
