public class FormulateMessage {
  
    public static String getFormulate(List<String> content){
        string message;
        List<Audit__c> a = [SELECT id, name_of_field__c, name_of_object__c, new_value__c ,old_value__c,type_of_change__c,type_of_object__c,date__c , user_audit__c from Audit__c WHERE id IN : content];
        for(Audit__c i : a){
        
        message = 'Type of change : '+i.type_of_change__c+'    Object ID : '+i.name_of_object__c+'   Object Type : '+i.type_of_object__c+' New Value : '+i.new_value__c+' Old Value :  '+ i.old_value__c+'  Date : '+i.date__c+'\n'+message;
        message=message.replace('null','');
        
        
        }
        return message;
        
        
    }
}
