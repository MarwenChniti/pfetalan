trigger Contract on Contract (after insert, before delete, after update) {
    List<Audit__c>listobj = new List<Audit__c>();
   
   
    /**********************
    TRIGGER BEFORE
    **********************/ 
    if(Trigger.isBefore){
       
        if ( Trigger.isDelete ){
            for (contract a : trigger.old){
                Auditfuntion f = new Auditfuntion();
                listobj.add(f.DeleteFunction(a)) ;
            }
           insert listobj ;
           
        }
        
        if(trigger.isInsert){
            for (contract a : trigger.new){
                Auditfuntion f = new Auditfuntion();
                listobj.add(f.DeleteFunction(a)) ;
            }
            insert listobj ;
        }
    }
   
   
   
    /**********************
    TRIGGER AFTER
    **********************/ 
    if(Trigger.isAfter){
       
        if(trigger.isUpdate){
           
            for (contract a : trigger.new){
                Auditfuntion f = new Auditfuntion();
                listobj = f.UpdateFunction(a);
            }            
            insert listobj ;

        }
       
        if(trigger.isInsert){
            for (contract a : trigger.new){
                Auditfuntion f = new Auditfuntion();
                listobj.add(f.CreateFunction(a)) ;
            }
            
    CustomNotificationType notificationType = [
        SELECT Id
        FROM CustomNotificationType
      ];
  
      // Create a new custom notification
      Messaging.CustomNotification notification = new Messaging.CustomNotification();
  
      // Set the contents for the notification
      notification.setTitle('Ajout contract');
      notification.setBody(
        UserInfo.getName() +
        ' a ajouté un contract ' 
      );
  
      // Set the notification type and target
      notification.setNotificationTypeId(notificationType.Id);
      notification.setTargetId(UserInfo.getUserId());
  
      // Actually send the notification
      Set<String> ls = new Set<String>();
      ls.add(UserInfo.getUserId());
      try {
        notification.send(ls);
      } catch (Exception e) {
        System.debug('Problem sending notification: ' + e.getMessage());
      }
            insert listobj ;
        }
       
    }  
    
    Boolean IsUpdate = Trigger.isUpdate;
    List <Opportunity> listeO = new List<Opportunity>();
    if (Trigger.isUpdate) {
        for (Contract c : Trigger.new){
            List <Opportunity> ls = [Select Id From Opportunity];
            for (Opportunity o : ls){
                if ((o.Id == c.Opportunity__c) && (c.Status == ('Accepted'))){
                    o.StageName = 'Closed Won';
                    listeO.add(o);
                }
            }
        }
        update(listeO);
    }


}