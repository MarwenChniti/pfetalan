trigger TransactionTrigger on APXT_CongaSign__Transaction__c (after insert, before delete, after update) {
    Boolean IsUpdate = Trigger.isUpdate;
    List <Contract> listeO = new List<Contract>();
    if (Trigger.isUpdate) {
        for (APXT_CongaSign__Transaction__c c : Trigger.new){
            List <Contract> ls = [Select Id From Contract];
            for (Contract o : ls){
                if ((o.Id == c.Parent_800__c) && (c.APXT_CongaSign__Status__c == ('COMPLETE'))){
                    o.Status = 'Accepted'; 
                    listeO.add(o);
                }
            }
        }
        update(listeO);
    }
}