import { LightningElement } from 'lwc';
import { NavigationMixin } from 'lightning/navigation';

export default class DashboardAdmin  extends NavigationMixin(LightningElement) {
    report1() {
        // Navigate to the Account home page
        this[NavigationMixin.Navigate]({
           //type: 'standard__objectPage',
           // attributes: {
                //objectApiName: 'Account',
                //actionName: 'new',
      
               type: 'standard__navItemPage',
               attributes: {
                   apiName: 'OppDashboard'
            },
        });
      }
      report2() {
        // Navigate to the Account home page
        this[NavigationMixin.Navigate]({
           //type: 'standard__objectPage',
           // attributes: {
                //objectApiName: 'Account',
                //actionName: 'new',
      
               type: 'standard__navItemPage',
               attributes: {
                   apiName: 'Contract_Dahsbord'
            },
        });
      }

      report3() {
        // Navigate to the Account home page
        this[NavigationMixin.Navigate]({
           //type: 'standard__objectPage',
           // attributes: {
                //objectApiName: 'Account',
                //actionName: 'new',
      
               type: 'standard__navItemPage',
               attributes: {
                   apiName: 'Case_Dashboard'
            },
        });
      }

      report4() {
        // Navigate to the Account home page
        this[NavigationMixin.Navigate]({
           //type: 'standard__objectPage',
           // attributes: {
                //objectApiName: 'Account',
                //actionName: 'new',
      
               type: 'standard__navItemPage',
               attributes: {
                   apiName: 'Feedback_Dashboard'
            },
        });
      }

      report5() {
        // Navigate to the Account home page
        this[NavigationMixin.Navigate]({
           //type: 'standard__objectPage',
           // attributes: {
                //objectApiName: 'Account',
                //actionName: 'new',
      
               type: 'standard__navItemPage',
               attributes: {
                   apiName: 'Quote_Dashboard'
            },
        });
      }
}