import { LightningElement , track , api  } from 'lwc';
import {ShowToastEvent} from 'lightning/platformShowToastEvent';
import readCSVFileOpp from '@salesforce/apex/OpportunityController.readCSVFileOpp';
const COLUMNS = [
    
    { label: 'Opportunity Name', fieldName: 'Name', type: 'text' },
    { label: 'Stage', fieldName: 'StageName', type: '	picklist' },    
    { label: 'Close Date', fieldName: 'CloseDate' },
    { label: 'Amount', fieldName: 'Amount' },
    
];


export default class OppImport extends LightningElement {


    @track COLUMNS = COLUMNS;

    @track error;

    @track recordId ; 
@track data;
@api recordId;



//////////////////////////////// import 


    // accepted parameters
    get acceptedFormats() {
        return ['.csv'];
    }
    
    handleUploadFinished(event) {
        // Get the list of uploaded files
        const uploadedFiles = event.detail.files;
  
        // calling apex class
        readCSVFileOpp({idContentDocument : uploadedFiles[0].documentId})
        .then(result => {
            window.console.log('result ===> '+result);
            this.data = result;
            this.dispatchEvent(
                new ShowToastEvent({
                    title: 'Success',
                    message: 'Accounts are created based CSV file',
                    variant: 'success',
                }),
            );
        })
        .catch(error => {
            this.error = error;
            this.dispatchEvent(
                new ShowToastEvent({
                    title: 'Error!!',
                    message: JSON.stringify(error),
                    variant: 'error',
                }),
            );     
        })
  
    }

    get acceptedFormatssss() {
        return ['.xlsx'];
    }

   

}