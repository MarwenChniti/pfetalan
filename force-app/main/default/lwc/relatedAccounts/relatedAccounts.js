import { LightningElement,wire , track , api } from 'lwc';
import { NavigationMixin } from 'lightning/navigation';
import getAccounts from '@salesforce/apex/AccountController.getAccounts';
import {ShowToastEvent} from 'lightning/platformShowToastEvent';
import readCSV from '@salesforce/apex/LWCExampleController.readCSVFile';
import { deleteRecord } from 'lightning/uiRecordApi';
import { refreshApex } from '@salesforce/apex';

const DELAY = 300;
//import retrieveAccountDetail from '@salesforce/apex/AccountController.retrieveAccountDetail';

const columns = [
  { label: 'Name', fieldName: 'Name' },
  { label: 'Phone', fieldName: 'Phone', type: 'phone' },
  { label: 'Description', fieldName: 'Description', type: 'text' },
  { label: 'Website', fieldName: 'Website', type: 'url' },
  { label: 'Industry', fieldName: 'Industry', type: 'text' },
  {
    label: 'Action',
    type: 'button',
    typeAttributes: {
        label: 'View details',
        name: 'view_details'
    }
},
{
  type: 'button',
    typeAttributes: {
      name : 'delete',
      label: 'Delete',
      title: 'Delete',
      variant: 'destructive',
      class: 'scaled-down',
      
    }
  },
];

/// importt 

const COLUMNS = [
  { label: 'Name', fieldName: 'Name' }, 
  { label: 'Industry', fieldName: 'Industry' },
  { label: 'Rating', fieldName: 'Rating'}, 
  { label: 'Type', fieldName: 'Type'}, 
  { label: 'Website', fieldName: 'Website', type:'url'}
];

export default class RelatedAccounts extends NavigationMixin(LightningElement)  {
// delete 
  
@api objectApiName;
  
@track error;
    @track page = 1;  
    @track startingRecord = 1;
    @track endingRecord = 0; 
    @track pageSize = 8; 
    @track totalRecountCount = 0;
    @track totalPage = 0;
   
    @track items = []; 

  
    loading;
    connectedCallback() {
      //this.loading = true;
      this.stopLoading(500);
    }  
    @track wiredDataResult;
   /**
   * The stopLoading utility is used to control a consistant state experience for the user - it ensures that
   * we don't have a flickering spinner effect when the state is in flux.
   * @param {timeoutValue} timeoutValue
   */
    stopLoading(timeoutValue) {
      setTimeout(() => {
        refreshApex(this.wiredDataResult);
        
        this.loading = false;
      }, timeoutValue);
    }
  
  
  







  accountName='';
//columns = COLUMNS ;
@track COLUMNS = COLUMNS;
@track columns = columns; 
@track recordId ; 
@track data;
@api recordId;
    @track error;


@track accountList ;
@wire(getAccounts,{actName:'$accountName'}) 
retrieveAccouts(result){ 
  this.loading = true;
      this.stopLoading(500);
  this.wiredDataResult = result;
    if (result.data) 
    {
      this.accountList = result.data;
         
         this.items = this.accountList ;
         this.totalRecountCount = this.accountList.length; 
         this.totalPage = Math.ceil(this.totalRecountCount / this.pageSize); 
         
        this.accountList = this.items.slice(0,this.pageSize); 
         this.endingRecord = this.pageSize;
         this.columns = columns;
      }
   else if (result.error) {
           this.error = result.error;
           this.columns = undefined;
       }
   
    
}

previousHandler() {
    if (this.page > 1) {
        this.page = this.page - 1; //decrease page by 1
        this.displayRecordPerPage(this.page);
    }
  }
  //clicking on next button this method will be called
  nextHandler() {
    if((this.page<this.totalPage) && this.page !== this.totalPage){
        this.page = this.page + 1; //increase page by 1
        this.displayRecordPerPage(this.page);            
    }             
  }


  displayRecordPerPage(page){
    
    this.startingRecord = ((page -1) * this.pageSize) ;
    this.endingRecord = (this.pageSize * page);
  
    this.endingRecord = (this.endingRecord > this.totalRecountCount) 
                        ? this.totalRecountCount : this.endingRecord; 
  
     this.accountList = this.items.slice(this.startingRecord, this.endingRecord);
  
    this.startingRecord = this.startingRecord + 1;
  }    
     

handleclick(event){
  const row = event.detail.row ;
  const actionName = event.detail.action.name;  
  if ( actionName === 'view_details' ){
  this[NavigationMixin.Navigate]({
    type: 'standard__recordPage',
    attributes: {
        objectApiName: 'Account',
        recordId: row.Id,
        actionName: 'view'
    }
});
}else if ( actionName === 'delete') {
  const row = event.detail.row;


  console.log('test2'+row.Id);

  deleteRecord(row.Id)
            .then(() => {
              this.loading = true;
                  this.stopLoading(500);
                this.dispatchEvent(
                    new ShowToastEvent({
                        title: 'Success',
                        message: 'Record deleted',
                        variant: 'success'
                    })
                );
                // Navigate to a record home page after
                // the record is deleted, such as to the
                // contact home page
                this[NavigationMixin.Navigate]({
                    //type: 'standard__objectPage',
                    // attributes: {
                         //objectApiName: 'Account',
                         //actionName: 'new',
               
                        type: 'standard__navItemPage',
                        attributes: {
                            apiName: 'Account'
                     },
                 });
            })
            .catch(error => {
                this.dispatchEvent(
                    new ShowToastEvent({
                        title: 'Error deleting record',
                        message: error.body.message,
                        variant: 'error'
                    })
                );
            });
    
}
}


refrech() {
  // Navigate to the Account home page
  this[NavigationMixin.Navigate]({
     //type: 'standard__objectPage',
     // attributes: {
          //objectApiName: 'Account',
          //actionName: 'new',

         type: 'standard__navItemPage',
         attributes: {
             apiName: 'Account'
      },
  });
}


navigateToObjectHome() {
  // Navigate to the Account home page
  this[NavigationMixin.Navigate]({
     type: 'standard__objectPage',
      attributes: {
          objectApiName: 'Account',
          actionName: 'new',

        // type: 'standard__navItemPage',
        // attributes: {
            // apiName: 'export'
      },
  });
}
handleKeyChange(event){
  const searchString= event.target.value;
  window.clearTimeout(this.delayTimeout);
  this.delayTimeout = setTimeout(()=>{
      this.accountName =searchString;
  },DELAY);
}

  // this method validates the data and creates the csv file to download
  downloadCSVFile() {   
    let rowEnd = '\n';
    let csvString = '';
    // this set elminates the duplicates if have any duplicate keys
    let rowData = new Set();

    // getting keys from data
    this.accountList.forEach(function (record) {
        Object.keys(record).forEach(function (key) {
            rowData.add(key);
        });
    });

    // Array.from() method returns an Array object from any object with a length property or an iterable object.
    rowData = Array.from(rowData);
    
    // splitting using ','
    csvString += rowData.join(',');
    csvString += rowEnd;

    // main for loop to get the data based on key value
    for(let i=0; i < this.accountList.length; i++){
        let colValue = 0;

        // validating keys in data
        for(let key in rowData) {
            if(rowData.hasOwnProperty(key)) {
                // Key value 
                // Ex: Id, Name
                let rowKey = rowData[key];
                // add , after every value except the first.
                if(colValue > 0){
                    csvString += ',';
                }
                // If the column is undefined, it as blank in the CSV file.
                let value = this.accountList[i][rowKey] === undefined ? '' : this.accountList[i][rowKey];
                csvString += '"'+ value +'"';
                colValue++;
            }
        }
        csvString += rowEnd;
    }

    // Creating anchor element to download
    let downloadElement = document.createElement('a');

    // This  encodeURI encodes special characters, except: , / ? : @ & = + $ # (Use encodeURIComponent() to encode these characters).
    downloadElement.href = 'data:text/csv;charset=utf-8,' + encodeURI(csvString);
    downloadElement.target = '_self';
    // CSV File Name
    downloadElement.download = 'Account Data.csv';
    // below statement is required if you are using firefox browser
    document.body.appendChild(downloadElement);
    // click() Javascript function to download CSV file
    downloadElement.click(); 
}

//////////////////////////////// import 


    // accepted parameters
    get acceptedFormats() {
      return ['.csv'];
  }
  
  handleUploadFinished(event) {
      // Get the list of uploaded files
      const uploadedFiles = event.detail.files;

      // calling apex class
      readCSV({idContentDocument : uploadedFiles[0].documentId})
      .then(result => {
          window.console.log('result ===> '+result);
          this.data = result;
          this.dispatchEvent(
              new ShowToastEvent({
                  title: 'Success',
                  message: 'Accounts are created based CSV file',
                  variant: 'success',
              }),
          );
      })
      .catch(error => {
          this.error = error;
          this.dispatchEvent(
              new ShowToastEvent({
                  title: 'Error!!',
                  message: JSON.stringify(error),
                  variant: 'error',
              }),
          );     
      })

  }
  upload() {
    // Navigate to the Account home page
    this[NavigationMixin.Navigate]({
       //type: 'standard__objectPage',
       // attributes: {
            //objectApiName: 'Account',
            //actionName: 'new',
  
           type: 'standard__navItemPage',
           attributes: {
               apiName: 'Import_Accounts'
        },
    });
  }

}