import { LightningElement } from 'lwc';
import {loadScript} from "lightning/platformResourceLoader";
import JSPDF from '@salesforce/resourceUrl/PDF';
import getAuditss from '@salesforce/apex/Audit.getAuditss'
export default class JSPFDemo extends LightningElement {

	contactList = [];
	headers = this.createHeaders([
		
		
        "type_of_change__c",
        "type_of_object__c"
       

	]);

	renderedCallback() {
     // loadScript(this, JSPDF);

		Promise.all([
			loadScript(this, JSPDF)
		]);
	}

	generatePdf(){
		const { jsPDF } = window.jspdf;
		const doc = new jsPDF();

		doc.text("Hi I'm Matt", 20, 20);
		doc.table(30, 30, this.contactList, this.headers, { autosize:true });
		doc.save("demo.pdf");
	}

	generateData(){
		getAuditss().then(result=>{
           // console.log(result);
			this.contactList = result;
			this.generatePdf();
		});
	}

	createHeaders(keys) {
		var result = [];
		for (var i = 0; i < keys.length; i += 1) {
			result.push({
				id: keys[i],
				name: keys[i],
				prompt: keys[i],
				width: 35,
				align: "center",
				padding: 0
			});
		}
		return result;
	}


}