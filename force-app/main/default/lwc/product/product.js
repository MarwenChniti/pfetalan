import { LightningElement, wire , track } from 'lwc';
import getProductList from '@salesforce/apex/ProductController.getProductList';
import { NavigationMixin } from 'lightning/navigation';

export default class Product  extends NavigationMixin(LightningElement) {

    @wire(getProductList) products;
    @track recordId ;

    
    handleclick(event){
        const row = event.detail.row ;
        console.log('test2'+row.Id);
        this[NavigationMixin.Navigate]({
          type: 'standard__recordPage',
          attributes: {
              objectApiName: 'Product2',
              recordId: row.Id,
              actionName: 'view'
          }
      });
      }
    
      New(){
    
        this[NavigationMixin.Navigate]({
          type: 'standard__objectPage',
          attributes: {
              objectApiName: 'Product2',
              actionName: 'new'
          }
      });
      }
}