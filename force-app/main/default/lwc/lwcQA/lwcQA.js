import { LightningElement, api, track, wire } from 'lwc';
import  changeOppDevReason from '@salesforce/apex/OpportunityController.changeOppDevReason';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { refreshApex } from '@salesforce/apex';
import  getRelatedOpportunities from '@salesforce/apex/OpportunityController.getRelatedOpportunities';

export default class LwcQA extends LightningElement {
    @track outputText;
    @api recordId;
    loading;
    @track wiredDataResult;

    @wire(getRelatedOpportunities,{searchKey: '$searchKey'})
    contacts(result) {
      this.wiredDataResult = result;
      if (result.data) 
      {
        this.allActivitiesData = result.data;
           
           this.items = this.allActivitiesData ;
           this.totalRecountCount = this.allActivitiesData.length; 
           this.totalPage = Math.ceil(this.totalRecountCount / this.pageSize); 
           
          this.allActivitiesData = this.items.slice(0,this.pageSize); 
           this.endingRecord = this.pageSize;
           this.columns = columns;
        }
     else if (result.error) {
             this.error = result.error;
             this.columns = undefined;
         }
     
    }

    updateText(event){
        this.outputText = this.template.querySelector('lightning-input').value;
        changeOppDevReason({oppId : this.recordId , reason : this.outputText})
        .then(result => {
            this.loading = true;
            this.stopLoading(500);
          }).then(()=> {
            const evt = new ShowToastEvent({
                title: 'Success',
                message: 'Reason saved',
                variant: 'info',
            });
            this.dispatchEvent(evt);
        }).then(()=> {
            const closeQA = new CustomEvent('close');
            this.dispatchEvent(closeQA);
        }) .catch (error => {
            this.error = error;
        });
    }
    /**
   * The stopLoading utility is used to control a consistant state experience for the user - it ensures that
   * we don't have a flickering spinner effect when the state is in flux.
   * @param {timeoutValue} timeoutValue
   */
  stopLoading(timeoutValue) {
    setTimeout(() => {
      refreshApex(this.wiredDataResult);
      this.loading = false;
    }, timeoutValue);
  }
}