import { LightningElement , api , track } from 'lwc';
import {ShowToastEvent} from 'lightning/platformShowToastEvent';
import readCSVFileLead from '@salesforce/apex/ProductController.readCSVFileProduct';

const COLUMNS = [
    { label: 'Product Name', fieldName: 'Name' },
    { label: 'Description', fieldName: 'Description', type: 'Text Area' },
    { label: 'ProductCode', fieldName: 'ProductCode' },
    { label: 'Image', fieldName: 'image__c' },
];


export default class ProductImport extends LightningElement {
    @track COLUMNS = COLUMNS;

    @track error;

    @track recordId ; 
@track data;
@api recordId;
loading;


//////////////////////////////// import 


    // accepted parameters
    get acceptedFormats() {
        return ['.csv'];
    }
    
    handleUploadFinished(event) {
        // Get the list of uploaded files
        const uploadedFiles = event.detail.files;
  
        // calling apex class
        readCSVFileLead({idContentDocument : uploadedFiles[0].documentId})
        .then(result => {
            window.console.log('result ===> '+result);
            this.data = result;
            this.dispatchEvent(
                new ShowToastEvent({
                    title: 'Success',
                    message: 'Accounts are created based CSV file',
                    variant: 'success',
                }),
            );
        })
        .catch(error => {
            this.error = error;
            this.dispatchEvent(
                new ShowToastEvent({
                    title: 'Error!!',
                    message: JSON.stringify(error),
                    variant: 'error',
                }),
            );     
        })
  
    }

    get acceptedFormatssss() {
        return ['.xlsx'];
    }

    upload() {
        // Navigate to the Account home page
        this[NavigationMixin.Navigate]({
           //type: 'standard__objectPage',
           // attributes: {
                //objectApiName: 'Account',
                //actionName: 'new',
      
               type: 'standard__navItemPage',
               attributes: {
                   apiName: 'Import_Products'
            },
        });
      }
}