import { LightningElement, track ,wire } from 'lwc';
import  getRelatedTransactions from '@salesforce/apex/ContractController.getRelatedTransactions';
import { NavigationMixin } from 'lightning/navigation';

const DELAY = 300;
const columns = [
    
    { label: 'Transaction Name', fieldName: 'Name' },
    { label: 'Contract Number', fieldName: 'Contract_Number__c' },
    { label: 'Status', fieldName: 'APXT_CongaSign__Status__c', cellAttributes:{
        class:{fieldName:'ChangeColor'}
        }},
    { label: 'Created On', fieldName: 'APXT_CongaSign__RequestedOn__c', type: 'date/time' },
    { label: 'Completed On', fieldName: 'APXT_CongaSign__CompletedOn__c', type: 'date/time' },
   
   
    
    {
      label: 'Action',
      type: 'button',
      typeAttributes: {
          label: 'View details',
          name: 'view_details'
      }
    },
];

export default class TransactionContracts  extends NavigationMixin(LightningElement) {

  
    @track page = 1;  
    @track startingRecord = 1;
    @track endingRecord = 0; 
    @track pageSize = 5; 
    @track totalRecountCount = 0;
    @track totalPage = 0;
   
    @track items = []; 
  
  
  
    searchKey = '';
  @track data ;
      @track allActivitiesData;
      @track columns = columns;
      @wire(getRelatedTransactions,{searchKey: '$searchKey'})
      contacts({error,data}) {
        if (data) 
        {
            console.log("test");
          this.allActivitiesData = data.map(item=>{
            let ChangeColor ;
            if(item.APXT_CongaSign__Status__c == 'SENT'){
                    ChangeColor="slds-text-color_error";
            }else {
                ChangeColor="slds-text-color_success";
            }
            
           
           
        
            return{...item,"ChangeColor":ChangeColor}
        })
          
             
             this.items = this.allActivitiesData ;
             this.totalRecountCount = this.allActivitiesData.length; 
             this.totalPage = Math.ceil(this.totalRecountCount / this.pageSize); 
             
            this.allActivitiesData = this.items.slice(0,this.pageSize); 
             this.endingRecord = this.pageSize;
             this.columns = columns;
          }
       else if (error) {
               this.error = error;
               this.columns = undefined;
           }
       
      }
  
      previousHandler() {
        if (this.page > 1) {
            this.page = this.page - 1; //decrease page by 1
            this.displayRecordPerPage(this.page);
        }
      }
      
      //clicking on next button this method will be called
      nextHandler() {
        if((this.page<this.totalPage) && this.page !== this.totalPage){
            this.page = this.page + 1; //increase page by 1
            this.displayRecordPerPage(this.page);            
        }             
      }
      
      //this method displays records page by page
        displayRecordPerPage(page){
      
        this.startingRecord = ((page -1) * this.pageSize) ;
        this.endingRecord = (this.pageSize * page);
      
        this.endingRecord = (this.endingRecord > this.totalRecountCount) 
                            ? this.totalRecountCount : this.endingRecord; 
      
         this.allActivitiesData = this.items.slice(this.startingRecord, this.endingRecord);
      
        this.startingRecord = this.startingRecord + 1;
      }    
         
  
  
      @track recordId ; 
  
      handleclick(event){
        const row = event.detail.row ;
        this[NavigationMixin.Navigate]({
          type: 'standard__recordPage',
          attributes: {
              objectApiName: 'APXT_CongaSign__Transaction__c',
              recordId: row.Id,
              actionName: 'view'
          }
      });
      }
  
  
      handleKeyChange(event) {
        
        window.clearTimeout(this.delayTimeout);
        const searchKey = event.target.value;
        this.delayTimeout = setTimeout(() => {
            this.searchKey = searchKey;
        }, DELAY);
    }
  
    New(){
      
      this[NavigationMixin.Navigate]({
        type: 'standard__objectPage',
        attributes: {
            objectApiName: 'Contract',
            actionName: 'new'
        }
    });
    }
  
    refrech() {
      // Navigate to the Account home page
      this[NavigationMixin.Navigate]({
         //type: 'standard__objectPage',
         // attributes: {
              //objectApiName: 'Account',
              //actionName: 'new',
    
             type: 'standard__navItemPage',
             attributes: {
                 apiName: 'Opportunities'
          },
      });
    }
  
    downloadCSVFile() {   
      let rowEnd = '\n';
      let csvString = '';
      // this set elminates the duplicates if have any duplicate keys
      let rowData = new Set();
  
      // getting keys from data
      this.allActivitiesData.forEach(function (record) {
          Object.keys(record).forEach(function (key) {
              rowData.add(key);
          });
      });
  
      // Array.from() method returns an Array object from any object with a length property or an iterable object.
      rowData = Array.from(rowData);
      
      // splitting using ','
      csvString += rowData.join(',');
      csvString += rowEnd;
  
      // main for loop to get the data based on key value
      for(let i=0; i < this.allActivitiesData.length; i++){
          let colValue = 0;
  
          // validating keys in data
          for(let key in rowData) {
              if(rowData.hasOwnProperty(key)) {
                  // Key value 
                  // Ex: Id, Name
                  let rowKey = rowData[key];
                  // add , after every value except the first.
                  if(colValue > 0){
                      csvString += ',';
                  }
                  // If the column is undefined, it as blank in the CSV file.
                  let value = this.allActivitiesData[i][rowKey] === undefined ? '' : this.allActivitiesData[i][rowKey];
                  csvString += '"'+ value +'"';
                  colValue++;
              }
          }
          csvString += rowEnd;
      }
  
      // Creating anchor element to download
      let downloadElement = document.createElement('a');
  
      // This  encodeURI encodes special characters, except: , / ? : @ & = + $ # (Use encodeURIComponent() to encode these characters).
      downloadElement.href = 'data:text/csv;charset=utf-8,' + encodeURI(csvString);
      downloadElement.target = '_self';
      // CSV File Name
      downloadElement.download = 'Contracts Status Data.csv';
      // below statement is required if you are using firefox browser
      document.body.appendChild(downloadElement);
      // click() Javascript function to download CSV file
      downloadElement.click(); 
  }
  
  
}