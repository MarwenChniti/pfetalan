import { LightningElement , track , api } from 'lwc';
import readCSVFileContact from '@salesforce/apex/ContactController.readCSVFileContact';
import {ShowToastEvent} from 'lightning/platformShowToastEvent';


const COLUMNS = [
   
    { label: 'Name', fieldName: 'LastName'  ,type :'text'}, 
    { label: 'Phone', fieldName: 'Phone' },
    { label: 'Email', fieldName: 'Email'}, 
    
   
  
  ];

export default class ContactImport extends LightningElement {

    @track COLUMNS = COLUMNS;

    @track error;

    @track recordId ; 
@track data;
@api recordId;

//////////////////////////////// import 


    // accepted parameters
    get acceptedFormats() {
        return ['.csv'];
    }
    
    handleUploadFinished(event) {
        // Get the list of uploaded files
        const uploadedFiles = event.detail.files;
  
        // calling apex class
        readCSVFileContact({idContentDocument : uploadedFiles[0].documentId})
        .then(result => {
            window.console.log('result ===> '+result);
            this.data = result;
            this.dispatchEvent(
                new ShowToastEvent({
                    title: 'Success',
                    message: 'Accounts are created based CSV file',
                    variant: 'success',
                }),
            );
        })
        .catch(error => {
            this.error = error;
            this.dispatchEvent(
                new ShowToastEvent({
                    title: 'Error!!',
                    message: JSON.stringify(error),
                    variant: 'error',
                }),
            );     
        })
  
    }

   


}